﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FInalAsignmentGrp4
{
	public class Coord
	{
		public int x;
		public int y;
		private int v;
		private int level;

		//default constructor to remove error in the coordinate method
		public Coord()
		{
		}

		//instance constructor
		public Coord(int v, int level)
		{
			this.v = v;
			this.level = level;
		}
	}

	//allows the calculations and results below to be accessed openly
	public static class GlobalMembers
	{

		//method for coordinates, taking into consideration the 6 sides of hexagon and possible moves from each side
		public static Coord CoordFromNum(int n)
		{
			const int turnCount = 6;
			Coord[] turns =
			{
				new Coord(){x = -1, y = 0},
				new Coord(){x = 0, y = -1},
				new Coord(){x = 1, y = -1},
				new Coord(){x = 1, y = 0},
				new Coord(){x = 0, y = 1},
				new Coord(){x = -1, y = 1},
			};
			int level = 0;    //level as the "base point"
			int levelStart = 1;  //first movement
			int levelEnd = 1;  //end point of the movement

			while (n > levelEnd)
			{
				levelStart = levelEnd;
				++level;
				levelEnd += level * 6;
			}

			Coord result = new Coord(0, level);
			for (int i = 0; i != turnCount; i++)
			{
				for (int j = 0; j != level; j++)
				{
					if (levelStart == n)
					{
						return result;
					}
					result.x += turns[i].x;
					result.y += turns[i].y;
					++levelStart;
				}
			}
			return result;
		}

		//method to get movement from input a and input b
		public static int getMove(int a, int b)
		{
			if (a < b)
			{
				return 1;
			}
			else if (a > b)
			{
				return -1;
			}
			return 0;
		}

		//method to get distance between cells, start is initial, target is end cell
		public static int getDistance(Coord start, Coord target)
		{
			int distance = 0;
			while (!(start.x == target.x && start.y == target.y))
			{
				++distance;
				int xInc = getMove(start.x, target.x);
				int yInc = getMove(start.y, target.y);
				start.x += xInc;
				if (Math.Abs(xInc + yInc) != 2)
				{
					start.y += yInc;
				}
			}
			return distance;
		}
	}
}
